﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class Adiciona_EstadoGovernador
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Governador g = new Governador();
                    Console.WriteLine("Digite o Nome do Governador ");
                    g.Nome = Console.ReadLine();

                    Estado e = new Estado();
                    Console.WriteLine("Digite o Nome do Estado");
                    e.Nome = Console.ReadLine();

                    e.Governador = g;

                    ctx.Estados.Add(e);
                    ctx.SaveChanges();

                    Console.WriteLine("Governador Cadastrado com o ID: " + g.Id);
                    Console.WriteLine("Estado Cadastrado com o ID: " + e.Id);
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao gravar dados " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
