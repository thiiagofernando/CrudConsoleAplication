﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class AdicionaDepartamentoFuncionario
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Funcionario f1 = new Funcionario();
                    Console.WriteLine("Digite o Nome do Primeiro Funcionario ");
                    f1.Nome = Console.ReadLine();

                    Funcionario f2 = new Funcionario();
                    Console.WriteLine("Digite o Nome do Segundo Funcionario");
                    f2.Nome = Console.ReadLine();

                    Departamento d = new Departamento();
                    Console.WriteLine("Digite O Nome do Departamento");
                    d.Nome = Console.ReadLine();

                    d.Funcionarios = new List<Funcionario>();

                    d.Funcionarios.Add(f1);
                    d.Funcionarios.Add(f2);

                    ctx.Departamentos.Add(d);
                    ctx.SaveChanges();

                    Console.WriteLine("");
                    Console.WriteLine("Primeiro Funcionario cadastrado com o Id: " + f1.Id + " Nome: " + f1.Nome);
                    Console.WriteLine("Segundo Funcionario cadastrado com o Id: " + f2.Id + " Nome: " + f2.Nome);
                    Console.WriteLine("departamento cadastrado com o Id: " + d.Id + " Nome: " + d.Nome);
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao gravar dados: " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
