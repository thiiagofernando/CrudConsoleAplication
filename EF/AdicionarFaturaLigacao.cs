﻿using System;
using System.Collections.Generic;

namespace EF
{
    class AdicionarFaturaLigacao
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Ligacao ligacao1 = new Ligacao();
                    Console.WriteLine("Digite o tempo da primeira ligação ");
                    ligacao1.Duracao = Convert.ToInt32(Console.ReadLine());

                    Ligacao ligacao2 = new Ligacao();
                    Console.WriteLine("Digite o tempo da segunda ligação");
                    ligacao2.Duracao = Convert.ToInt32(Console.ReadLine());

                    Fatura fatura = new Fatura();
                    fatura.Ligacao = new List<Ligacao>();

                    ligacao1.Fatura = fatura;
                    ligacao2.Fatura = fatura;

                    fatura.Ligacao.Add(ligacao1);
                    fatura.Ligacao.Add(ligacao2);

                    ctx.Ligacoes.Add(ligacao1);
                    ctx.Ligacoes.Add(ligacao2);
                    ctx.Faturas.Add(fatura);

                    ctx.SaveChanges();

                    Console.WriteLine();
                    Console.WriteLine("Primeira Ligação Cadastrada com ID: " + ligacao1.Id);
                    Console.WriteLine("Segunda Ligação Cadastrada com ID: " + ligacao2.Id);
                    Console.WriteLine("Fatura Cadastrada com ID: " + fatura.Id);
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao gravar dados: " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
