﻿using System;
using System.Collections.Generic;

namespace EF
{
    class AdicionarLivroAutor
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Autor autor = new Autor();
                    Console.WriteLine("Digite o Nome do Autor");
                    autor.Nome = Console.ReadLine();

                    Livro livro = new Livro();
                    Console.WriteLine("Digite o Nome do Livro");
                    livro.Nome = Console.ReadLine();

                    autor.Livros = new List<Livro>();
                    livro.Autores = new List<Autor>();

                    autor.Livros.Add(livro);
                    livro.Autores.Add(autor);

                    ctx.Livros.Add(livro);
                    ctx.SaveChanges();

                    Console.WriteLine();
                    Console.WriteLine("Autor Cadastrado com ID: " + autor.Id);
                    Console.WriteLine("Livro Cadastrado com ID: " + livro.Id);
                    Console.WriteLine("Dados Gravos com Sucesso!! ");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao gravar dados " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
