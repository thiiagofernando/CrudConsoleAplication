﻿using System;
namespace EF
{
    class AdicionarPedidoConsumidor
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Consumidor c = new Consumidor();
                    Console.WriteLine("Digite o Nome do Consumidor");
                    c.Nome = Console.ReadLine();

                    Pedido p = new Pedido();

                    p.Consumidor = c;

                    ctx.Pedidos.Add(p);
                    ctx.SaveChanges();

                    Console.WriteLine("Consumidor Cadastrado com o ID " + c.Id);
                    Console.WriteLine("Pedido Cadastrado com Id " + p.Id);
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao Gravar dados: " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
