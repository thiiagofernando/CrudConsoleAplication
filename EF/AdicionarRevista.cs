﻿using System;

namespace EF
{
    class AdicionarRevista
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    for (int i = 0; i < 15; i++)
                    {
                        Revista revista = new Revista();
                        revista.Nome = "Revista " + (i + 1);
                        revista.Preco = 10.0 * (i + 1);
                        ctx.Revista.Add(revista);
                    }
                    ctx.SaveChanges();
                    Console.WriteLine("Sucesso na operação");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Falha da Operação " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
