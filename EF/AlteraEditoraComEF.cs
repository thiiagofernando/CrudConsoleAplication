﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class AlteraEditoraComEF
    {
        static void Main(string[] args)
        {
            try
            {
                using (EFContext ctx = new EFContext())
                {
                    Console.WriteLine("Digite o Codigo da Editora que deseja alterar");
                    long id = Convert.ToInt64(Console.ReadLine());

                    Editora e = ctx.Editoras.Find(id);

                    Console.WriteLine("Digite o Novo Nome da Editora");
                    e.Nome = Console.ReadLine();

                    Console.WriteLine("Digite o novo Email  da Editora");
                    e.Email = Console.ReadLine();

                    ctx.SaveChanges();

                    Console.WriteLine("Dados Atualizados com Sucesso!!!");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falha ao atualizar dados devo ao erro  :" + ex.Message);
                Console.ReadKey();
            }
        }
    }
}
