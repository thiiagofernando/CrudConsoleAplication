﻿using System.Collections.Generic;


namespace EF
{
    class Autor
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public ICollection<Livro> Livros { get; set; }
    }
}
