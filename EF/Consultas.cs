﻿using System;
using System.Linq;

namespace EF
{
    class Consultas
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                /*Recuperando a quantidade de revistas*/
                int contador = ctx.Revista.Count();

                Console.WriteLine("Há " + contador + " revista.\n");

                /*Recuperando a quantidade de revistas com preço acima de 100*/
                contador = ctx.Revista.Count(x => x.Preco > 100);

                Console.WriteLine("Há " + contador + " revistas com preço acima de 10.\n");

                /*Somando preoços das revistas*/
                double soma = ctx.Revista.Sum(x => x.Preco);
                Console.WriteLine("A soma dos preços  das revistas é " + soma + " \n");

                /*Recuperando preço da revista mais cara*/
                double precoMaximo = ctx.Revista.Max(x => x.Preco);
                Console.WriteLine("O Preço da revista mais cara é  " + precoMaximo + "\n");

                /*recuperando a media dos preços das revistas*/
                double media = ctx.Revista.Average(x => x.Preco);
                Console.WriteLine("Media do preço das revistas  " + media + "\n");

                /*Recuperando todas as revistas*/
                var revistas = ctx.Revista;
                Console.WriteLine("Todas Revistas: ");

                foreach (Revista revista in revistas)
                {
                    Console.WriteLine("Id: " + revista.Id);
                    Console.WriteLine("Nome: " + revista.Nome);
                    Console.WriteLine("Preço: " + revista.Preco);
                    Console.WriteLine();
                }

                /*Recuperando as revistas com preço acima de 100*/
                var revistasCaras = ctx.Revista.Where(x => x.Preco > 100);
                Console.WriteLine("Revistas com preço  acima de 100 ");

                foreach (Revista revista in revistasCaras)
                {
                    Console.WriteLine("Id: " + revista.Id);
                    Console.WriteLine("Nome: " + revista.Nome);
                    Console.WriteLine("Preço: " + revista.Preco);
                }

                Console.ReadKey();
            }
        }
    }
}
