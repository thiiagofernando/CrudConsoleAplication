﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EF
{
    class DespesaRepositorio
    {
        private GranaContext ctx;

        public DespesaRepositorio(GranaContext ctx)
        {
            this.ctx = ctx;
        }

        public void Adiciona(Despesas despesa)
        {
            ctx.Despesas.Add(despesa);
        }

        public void Remove(Despesas despesa)
        {
            ctx.Despesas.Remove(despesa);
        }

        public double? SomaDespesasAte(DateTime data)
        {
            return ctx.Despesas
                .Where(x => x.Data <= data)
                .Sum(x => x.Valor);
        }

        public double? SomaDespesas(
            DateTime dataInicial,
            DateTime dataFinal
            )
        {
            return ctx.Despesas
                .Where(x => x.Data >= dataInicial
                && x.Data <= dataFinal)
                .Sum(x => x.Valor);
        }

        public List<Despesas> BuscarPorPedido(
            DateTime dataInicial,
            DateTime dataFinal)
        {
            return ctx.Despesas
                .Where(x => x.Data >= dataInicial
                && x.Data <= dataFinal)
                .ToList();
        }

        public List<Despesas> BuscaRecentes()
        {
            return ctx.Despesas
                .OrderByDescending(x => x.Id)
                .Take(10)
                .ToList();
        }
    }
}
