﻿

namespace EF
{
    class Estado
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public Governador Governador { get; set; }
    }
}
