﻿using System.Collections.Generic;


namespace EF
{
    class Fatura
    {
        public long Id { get; set; }
        public ICollection<Ligacao> Ligacao { get; set; }
    }
}
