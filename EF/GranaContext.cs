﻿using System.Data.Entity;

namespace EF
{
    class GranaContext : DbContext
    {
        public DbSet<Despesas> Despesas { get; set; }
        public DbSet<Receita> Receitas { get; set; }

        public GranaContext()
        {
            DropCreateDatabaseIfModelChanges<GranaContext> initializer =
                new DropCreateDatabaseIfModelChanges<GranaContext>();
            Database.SetInitializer<GranaContext>(initializer);
        }
    }
}