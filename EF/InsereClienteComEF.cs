﻿using System;
namespace EF
{
    class InsereClienteComEF
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                try
                {
                    Cliente c = new Cliente();

                    Console.Write("Digite o Nome do Cliente: ");
                    c.Nome = Console.ReadLine();

                    Console.Write("Digite o CPF do Cliente: ");
                    c.Cpf = Console.ReadLine();

                    Endereco e = new Endereco();
                    c.Endereco = e;
                    Console.WriteLine("Digite o Endereço do Cliente: ");

                    Console.Write("Pais: ");
                    e.Pais = Console.ReadLine();

                    Console.Write("Estado: ");
                    e.Estado = Console.ReadLine();

                    Console.Write("Cidade: ");
                    e.Cidade = Console.ReadLine();

                    Console.Write("Logradouro: ");
                    e.Logradouro = Console.ReadLine();

                    Console.Write("Nº: ");
                    e.Numero = Console.ReadLine();

                    Console.Write("Complemento: ");
                    e.Complemento = Console.ReadLine();

                    Console.Write("Cep: ");
                    e.Cep = Console.ReadLine();

                    ctx.Clientes.Add(c);
                    ctx.SaveChanges();

                    Console.WriteLine("Cliente Castrado com o ID: " + c.Id);
                    Console.ReadKey();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Falha ao gravar os dados " + ex.Message);
                    Console.ReadKey();
                }


            }
        }
    }
}
