﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class InsereEditoraComEF
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                Editora e = new Editora();

                Console.Write("Informe o Nome da Editora: ");
                e.Nome = Console.ReadLine();

                Console.Write("Informe o Email da Editora: ");
                e.Email = Console.ReadLine();


                ctx.Editoras.Add(e);

                ctx.SaveChanges();

                Console.WriteLine("Editora Cadastrada com o id: " + e.Id);
                Console.ReadKey();
            }
        }
    }
}
