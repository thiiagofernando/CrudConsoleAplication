﻿using System.Collections.Generic;

namespace EF
{
    class Livro
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public ICollection<Autor> Autores { get; set; }
    }
}
