﻿using System;


namespace EF
{
    class Receita
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public double Valor { get; set; }
        public DateTime Data { get; set; }
        public string Tipo { get; set; }
        public override string ToString()
        {
            string s = this.Nome + " - ";
            s += this.Valor + " - ";
            s += this.Tipo + " - ";
            s += this.Data.ToString("dd/MM/yyyy");

            return s;
        }
    }
}
