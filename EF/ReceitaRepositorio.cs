﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class ReceitaRepositorio
    {
        private GranaContext ctx;

        public ReceitaRepositorio(GranaContext ctx)
        {
            this.ctx = ctx;
        }

        public void Adiciona(Receita receita)
        {
            ctx.Receitas.Add(receita);
        }

        public void Remove(Receita receita)
        {
            ctx.Receitas.Remove(receita);
        }
        public double? SomaReceitasAte(DateTime data)
        {
            return ctx.Receitas
                .Where(x => x.Data <= data)
                .Sum(x => x.Valor);
        }
        public List<Receita> BuscaPorPeriodo(
            DateTime dataInicial,
            DateTime dataFinal
            )
        {
            return ctx.Receitas
                .Where(x => x.Data >= dataInicial
                && x.Data <= dataFinal)
                .ToList();
        }
        public List<Receita> BuscaRecentes()
        {
            return ctx.Receitas
                .OrderByDescending(x => x.Id)
                .Take(10)
                .ToList();
        }
    }
}
