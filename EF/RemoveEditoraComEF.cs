﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class RemoveEditoraComEF
    {
        static void Main(string[] args)
        {
            try
            {
                using (EFContext ctx = new EFContext())
                {
                    Console.WriteLine("Digite o ID da Editora que deseja remover");
                    long id = Convert.ToInt64(Console.ReadLine());

                    Editora e = ctx.Editoras.Find(id);

                    ctx.Editoras.Remove(e);

                    ctx.SaveChanges();
                }

                Console.WriteLine("Editora Excluida com Sucesso!!!!");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falha ao Excluir ou Editora não existe " + ex.Message);
                Console.ReadKey();
            }
        }
    }
}
