﻿namespace EF
{
    class Revista
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public double Preco { get; set; }
    }
}
