﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class TelaGeraDados : Tela
    {
        private Tela anterior;
        public string Nome { get; set; }

        public TelaGeraDados(Tela anterior)
        {
            this.anterior = anterior;
            this.Nome = "Gera Dados";
        }

        public Tela Mostra()
        {

        }
    }
}
