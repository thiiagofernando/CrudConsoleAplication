﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
    class Teste
    {
        static void Main(string[] args)
        {
            using (EFContext ctx = new EFContext())
            {
                Editora e = new Editora
                {
                    Nome = "K19",
                    Email = "Contato@k19.com"
                };

                ctx.Editoras.Add(e);
                ctx.SaveChanges();
            }
        }
    }
}
