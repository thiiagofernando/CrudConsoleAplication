﻿using ODBC.ConexãoBD;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class CriaBaseDeDadosLivraria2
    {
        static void Main(string[] args)
        {

            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                conexao.Open();

                string sql = @"IF Exists (select name from master.dbo.sysdatabases where name = 'livraria')
                                drop database livraria";
                OdbcCommand command = new OdbcCommand(sql, conexao);
                command.ExecuteNonQuery();

                sql = @"create database livraria";
                command = new OdbcCommand(sql, conexao);
                command.ExecuteNonQuery();
            }
        }
    }
}
