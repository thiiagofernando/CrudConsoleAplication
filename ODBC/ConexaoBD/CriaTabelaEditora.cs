﻿using ODBC.ConexãoBD;
using System;
using System.Data.Odbc;
namespace ODBC
{
    class CriaTabelaEditora
    {
        static void Main(string[] args)
        {
            
            try
            {
                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    conexao.Open();

                    string sql =
                        "CREATE TABLE [dbo].[Editora](" +
                                "[id] [int] IDENTITY(1,1) NOT NULL," +
                                "[nome] [varchar](255) NOT NULL," +
                                "[email] [varchar](255) NOT NULL," +
                                 "CONSTRAINT [PK_Editoras] PRIMARY KEY CLUSTERED " +
                                "(" +
                                    "[id] ASC" +
                                ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" +
                                ") ON [PRIMARY]";
                    OdbcCommand command = new OdbcCommand(sql, conexao);
                    command.ExecuteNonQuery();

                }
                System.Console.Write("Tabela Editora Criada");
                System.Console.ReadKey();
            }
            catch (Exception ex)
            {

                System.Console.WriteLine("Falha ao Criar Tabela " + ex.Message);
                System.Console.ReadKey();
            }

              
        }
        
    }
}
