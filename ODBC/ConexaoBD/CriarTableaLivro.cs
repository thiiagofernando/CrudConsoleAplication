﻿using ODBC.ConexãoBD;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class CriarTableaLivro
    {
        static void Main(string[] args)
        {
            try
            {
          
                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    conexao.Open();

                    string sql =
                            "CREATE TABLE [dbo].[Livros](" +
                                "[IdLivro] [int] IDENTITY(1,1) NOT NULL," +
                                "[NomeLivro] [nvarchar](255) NOT NULL," +
                                "[Preco] [float] NOT NULL," +
                                "[IdEditora] [int] NOT NULL," +
                             "CONSTRAINT [PK_Livros] PRIMARY KEY CLUSTERED " +
                            "(" +
                                "[IdLivro] ASC" +
                            ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" +
                            ") ON [PRIMARY]" +
                            "ALTER TABLE [dbo].[Livros]  WITH CHECK ADD  CONSTRAINT [FK_Livros_Livros] FOREIGN KEY([IdEditora])" +
                            "REFERENCES [dbo].[Editora] ([id])" +
                            "ALTER TABLE [dbo].[Livros] CHECK CONSTRAINT [FK_Livros_Livros]";

                    OdbcCommand command = new OdbcCommand(sql, conexao);
                    command.ExecuteNonQuery();
                    System.Console.Write("Tabela Livro Criada");
                    System.Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fala ao Criar Tabela " + ex.Message);
                System.Console.ReadKey();
            }
        }


    }
}
