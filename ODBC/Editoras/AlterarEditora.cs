﻿using System;
using System.Data.Odbc;
using ODBC.ConexãoBD;

namespace ODBC
{
    class AlterarEditora
    {
        static void Main(string[] args)
        {
            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                try
                {
                    Editora e = new Editora();

                    Console.WriteLine("Digite o Id da Editora que Deseja Alterar");
                    e.Id = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Digite o Novo Nome da Editora: ");
                    e.Nome = Console.ReadLine();

                    Console.Write("Digite o Nome Email da Editora : ");
                    e.Email = Console.ReadLine();

                    string sql = "UPDATE Editora SET Nome = ?, Email= ? Where Id = ?";

                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@Nome", e.Nome);
                    command.Parameters.AddWithValue("@Email", e.Email);
                    command.Parameters.AddWithValue("@Id", e.Id);

                    conexao.Open();

                    command.ExecuteNonQuery();

                    Console.WriteLine("Dado atualizados com Sucesso!!!");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                      
                    Console.WriteLine("Falha ao atualizar dados " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
