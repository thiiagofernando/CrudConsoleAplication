﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class Editora
    {
        public int? Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
    }
}
