﻿using ODBC.ConexãoBD;
using System;
using System.Data.Odbc;

namespace ODBC
{
    class InsereEditora
    {

        static void Main(string[] args)
        {
            try
            {
                
                Editora e = new Editora();

                Console.Write("Digite o Nome da Editora : ");
                e.Nome = Console.ReadLine();

                Console.Write("Digite o Email da Editora : ");
                e.Email = Console.ReadLine();

                string sql =
                    @"insert into Editora (Nome, Email) 
                OUTPUT inserted.id
                values(?, ?)";

                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@Nome", e.Nome);
                    command.Parameters.AddWithValue("@Email", e.Email);


                    conexao.Open();
                    e.Id = command.ExecuteScalar() as int?;

                    Console.WriteLine("Editora Cadastrada com o ID: " + e.Id);
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine("Falha na hora de Inserir Dados " + ex.Message);
               Console.ReadKey();
            }
        }
    }
}
