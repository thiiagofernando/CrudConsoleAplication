﻿using System;
using System.Data.Odbc;
using System.Collections.Generic;
using ODBC.ConexãoBD;

namespace ODBC
{
    class ListaEditoras
    {
        static void Main(string[] args)
        {

            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                try
                {
                    string sql = "select * from  Editora";
                    OdbcCommand command = new OdbcCommand(sql, conexao);
                    conexao.Open();
                    OdbcDataReader resultado = command.ExecuteReader();

                    List<Editora> lista = new List<Editora>();

                    while (resultado.Read())
                    {
                        Editora e = new Editora();

                        e.Id = resultado["ID"] as int?;
                        e.Nome = resultado["Nome"] as string;
                        e.Email = resultado["Email"] as string;

                        lista.Add(e);
                    }
                    foreach (Editora e in lista)
                    {
                        Console.WriteLine("{0} : {1} - {2}\n", e.Id, e.Nome, e.Email);
                        
                    }
                    Console.ReadKey();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Falha ao Listar Dados: " + ex.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
