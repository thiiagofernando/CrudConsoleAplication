﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using ODBC.ConexãoBD;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class RemoverEditora
    {
        static void Main(string[] args)
        {
            try
            {
                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    Console.WriteLine("Digite o ID da Editora que Deseja Excluir");
                    int id = Convert.ToInt32(Console.ReadLine());

                    string sql = "DELETE From Editora Where id = ?";

                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@id", id);

                    conexao.Open();

                    command.ExecuteNonQuery();
                    Console.WriteLine("Dados Excluidos com Sucesso!!");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                
                Console.WriteLine("Falha ao Excluir Editora Existem Livros Vinculados a essa Editora: ");
                Console.ReadKey();
            }
        }
    }
}
