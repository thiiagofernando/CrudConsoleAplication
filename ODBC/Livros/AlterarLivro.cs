﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using ODBC.ConexãoBD;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class AlterarLivro
    {
        static void Main(string[] args)
        {
            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                try
                {
                    Livro l = new Livro();

                    Console.WriteLine("Digite o ID do Livro que Deseja alterar");
                    l.IdLivro = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Digite o Novo Nome do Livro");
                    l.NomeLivro = Console.ReadLine();

                    Console.WriteLine("Informe o novo valor do Livro");
                    l.Preco = Convert.ToDouble(Console.ReadLine());

                    Console.WriteLine("Inform o Novo Codigo da Editora");
                    l.IdEditora = Convert.ToInt32(Console.ReadLine());

                    string sql = "UPDATE Livros set NomeLivro = ? , Preco = ? , IdEditora = ? where IdLivro = ?";

                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@NomeLivro", l.NomeLivro);
                    command.Parameters.AddWithValue("@Preco", l.Preco);
                    command.Parameters.AddWithValue("@IdEditora", l.IdEditora);
                    command.Parameters.AddWithValue("@IdLivro", l.IdLivro);

                    conexao.Open();

                    command.ExecuteNonQuery();
                    Console.WriteLine("Dados Gravados com Sucesso!!!");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Falha ao gravar dados" + ex.Message);
                    Console.ReadKey();
                }

            }
        }
    }
}
