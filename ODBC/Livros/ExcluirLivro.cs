﻿using System;
using ODBC.ConexãoBD;
using System.Data.Odbc;


namespace ODBC.Livros
{
    class ExcluirLivro
    {
        static void Main(string[] args)
        {
            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                try
                {
                    Console.WriteLine("Informe o Codido do Livro que Deseja Excluir");
                    int IdLivro = Convert.ToInt32(Console.ReadLine());

                    string sql = "DELETE FROM Livros WHERE IdLivro = ?";

                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@IdLivro", IdLivro);

                    conexao.Open();

                    command.ExecuteNonQuery();
                    Console.WriteLine("Livro Excluido com Sucesso!!!!");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Falha ao Excluir Livro " + ex.Message);
                    Console.ReadKey();

                }
            }
        }
    }
}
