﻿using ODBC.ConexãoBD;
using System;
using System.Data.Odbc;
using System.Globalization;
using System.Threading;

namespace ODBC
{
    class InsereLivro
    {
        static void Main(string[] args)
        {
            try
            {
                
                Livro l = new Livro();

                Console.WriteLine("Digite o Nome do Livro : ");
                l.NomeLivro = Console.ReadLine();

                Console.WriteLine("Digite o Preço do Livro : ");
                l.Preco = Convert.ToDouble(Console.ReadLine());


                Console.WriteLine("Digite o Codigo da Editora : ");
                l.IdEditora = Convert.ToInt32(Console.ReadLine());

                string sql =
                    @"INSERT INTO Livros
                               (NomeLivro,Preco,IdEditora)
                               OUTPUT inserted.IdLivro
                         VALUES(?,?,?)";

                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    OdbcCommand command = new OdbcCommand(sql, conexao);

                    command.Parameters.AddWithValue("@Nome", l.NomeLivro);
                    command.Parameters.AddWithValue("@Preco", l.Preco);
                    command.Parameters.AddWithValue("@IdEditora", l.IdEditora);

                    conexao.Open();
                    l.IdLivro = command.ExecuteScalar() as int?;
                    Console.WriteLine("Livro Cadastrado com o Codigo : " + l.IdLivro);
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falha ao Inserir dados " + ex.Message);
                Console.ReadKey();
            }
        }
    }
}
