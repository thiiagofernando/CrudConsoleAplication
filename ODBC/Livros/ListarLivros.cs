﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using ODBC;
using ODBC.ConexãoBD;

namespace ODBC
{
    class ListarLivros
    {
        static void Main(string[] args)
        {
            try
            {

                using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
                {
                    string sql = "select * from Livros";
                    OdbcCommand command = new OdbcCommand(sql, conexao);
                    conexao.Open();
                    OdbcDataReader resultado = command.ExecuteReader();

                    List<Livro> lista = new List<Livro>();

                    while (resultado.Read())
                    {
                        Livro l = new Livro();

                        l.IdLivro = resultado["IdLivro"] as int?;
                        l.NomeLivro = resultado["NomeLivro"] as string;
                        l.Preco = resultado["Preco"] as double?;
                        l.IdEditora = resultado["IdEditora"] as int?;

                        lista.Add(l);
                    }
                    foreach (Livro l in lista)
                    {
                        Console.WriteLine("ID: {0} : Livro: {1} : Preço: {2} - Codigo Editora: {3}", l.IdLivro, l.NomeLivro, l.Preco, l.IdEditora);
                    }


                    Console.ReadKey();

                    
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falha ao Listar Dados: " + ex.Message);
                Console.ReadKey();
                
            }





        }



        

    }
   
}
