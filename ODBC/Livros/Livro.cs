﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBC
{
    class Livro
    {
        public int? IdLivro { get; set; }
        public string NomeLivro { get; set; }
        public double? Preco { get; set; }
        public int? IdEditora { get; set; }
    }
}
